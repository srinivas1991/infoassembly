package com.infoassembly;

import java.util.ArrayList;
import java.util.Arrays;

import com.origamilabs.library.views.StaggeredGridView;
import com.origamilabs.library.views.StaggeredGridView.OnItemClickListener;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;

public class PictureList extends Activity  implements OnItemClickListener{
	StaggeredAdapter adapter;
	ArrayList<String> urls;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.grid_layout);
		
        StaggeredGridView gridView = (StaggeredGridView) this.findViewById(R.id.staggeredGridView1);
		gridView.setOnItemClickListener(this);
		int margin=getResources().getDimensionPixelSize(R.dimen.margin);
		String[] temp=getIntent().getStringArrayExtra("picture");
		if(getIntent().getIntExtra("type", 1)==1)
		{
		urls=new ArrayList<String>(Arrays.asList(temp));
		}
		else
		{
	urls=getIntent().getStringArrayListExtra("picture");}
		String album_name=getIntent().getStringExtra("albumname");
		gridView.setItemMargin(margin); // set the GridView margin
		
		gridView.setPadding(margin, 0, margin, 0); // have the margin on the sides as well 
		
		 adapter = new StaggeredAdapter(PictureList.this, R.id.imageView1, urls,album_name);
		
		gridView.setAdapter(adapter);
		adapter.notifyDataSetChanged();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
	
	}
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
	
			//finish();	
	}
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		
	}

	@Override
	public void onItemClick(StaggeredGridView parent, View view, int position,
			long id) {
		
		Intent i=new Intent(PictureList.this,ImageHost.class);
	i.putExtra("url",urls.get(position) );
	startActivity(i);
	overridePendingTransition(R.anim.activity_slide_up,
			0);

		} 
}
