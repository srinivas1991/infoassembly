package com.infoassembly;

import java.io.InputStream;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

public class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
	  ImageView bmImage;
	  Context context;
	  ViewGroup v;

		private ProgressDialog pdia;
 
	  @Override
protected void onPreExecute() {
	// TODO Auto-generated method stub
	super.onPreExecute();
	pdia = new ProgressDialog(context);
	
	 pdia.show();
}
	  public DownloadImageTask(ImageView bmImage,Context contxt,ViewGroup vg) {
	      this.bmImage = bmImage;
	      this.context=contxt;
	      this.v=vg;
	  }

	  protected Bitmap doInBackground(String... urls) {
	      String urldisplay = urls[0];
	      System.out.println(urldisplay);
	      Bitmap mIcon11 = null;
	      try {
	        InputStream in = new java.net.URL(urldisplay).openStream();
	        mIcon11 = BitmapFactory.decodeStream(in);
	      } catch (Exception e) {
	    	 //	 Toast.makeText(context,"Error fetching image,Try Again",Toast.LENGTH_LONG).show();
	    		   
	    	  Log.e("Error", e.getMessage());
	          e.printStackTrace();
	      }
	      return mIcon11;
	       
	  }

	  protected void onPostExecute(Bitmap result) {
	     if(result!=null)
	     {  bmImage.setImageBitmap(result);
	    
	     Animation bottomUp = AnimationUtils.loadAnimation(context,
	             R.anim.activity_slide_up);

	 v.startAnimation(bottomUp);
	 v.setVisibility(View.VISIBLE);
	     }
	    
	    	// Toast.makeText(context,"Something went Wrong,Try Again",Toast.LENGTH_LONG).show();
	     // if (pdia != null)
	      pdia.dismiss();
		     
	      // pdia.dismiss();
	  }
	}