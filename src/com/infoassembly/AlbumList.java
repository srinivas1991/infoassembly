package com.infoassembly;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;

import android.app.ProgressDialog;
import android.app.TabActivity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.widget.TabHost.TabSpec;
import android.widget.TabHost;

public class AlbumList extends TabActivity {
	 ArrayList<String> albumid;
	 ArrayList<String> url;
	 ArrayList<String> alb;
	 String jsondata="";
	 private ProgressDialog pdia;
		public static TabHost tabHost;


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		  url=new ArrayList<String>();
		    
		// no more this
setContentView(R.layout.album_list);

  alb=new ArrayList<String>();
 albumid=new ArrayList<String>();
 alb.add("Profile Pictures");
 //alb=getIntent().getStringArrayListExtra("albums");
 albumid=getIntent().getStringArrayListExtra("albumid");
	tabHost = (TabHost)findViewById(android.R.id.tabhost);

    TabSpec img = tabHost.newTabSpec("tid1");
    TabSpec sample_app = tabHost.newTabSpec("tid2");
    TabSpec home = tabHost.newTabSpec("tid1");
    Intent i=new Intent().setClass(AlbumList.this,PictureList.class);
    Intent i2 = new Intent(this, SampleApp.class);
    Intent i3=new Intent(this,HomeActivity.class);
	

    if(getIntent().getIntExtra("type", 1)==1)
    {
	i.putExtra("picture",getIntent().getStringArrayExtra("urls"));
	i.putExtra("albumname","Google");
	i.putExtra("type", 1);
	i3.putExtra("data",getIntent().getStringArrayExtra("urls"));
	i3.putExtra("type", 1);
    }else
    {
    	i.putExtra("picture",url);
    	i.putExtra("albumname","ProfilePictures");
    	i.putExtra("type",0);
    	i3.putExtra("data",url);
    	i3.putExtra("type", 0);
    		
        	
    

    pdia = new ProgressDialog(this);
	pdia.setCancelable(true);
	pdia.setMessage("retrieving photos..");
	pdia.show();
	 getPictures(albumid.get(0));
 	
				
    }
    Resources res=getResources();
		  img.setIndicator("Images",res.getDrawable(R.drawable.image)).setContent(i);
		    tabHost.addTab(img);
		   
		    home.setIndicator("Home",res.getDrawable(R.drawable.hom)).setContent(i3);
		    tabHost.addTab(home);
		  
		    sample_app.setIndicator("Sample App",res.getDrawable(R.drawable.sample)).setContent(i2);
		    tabHost.addTab(sample_app);
		    tabHost.setCurrentTab(1);

	}
	void getPictures(String id) {
		Bundle b=new Bundle();
        b.putString("fields","source");
        b.putString("limit","100");
		Session session = Session.getActiveSession();
		   Request request = new Request(session,
		       "/"+id+"/photos",                         
		       b,                        
		       HttpMethod.GET,                 
		       new Request.Callback(){         
		           public void onCompleted(Response response) {
		        	   if(response.getError()==null)
		        	   {
		           
		        	   try { 
		            	JSONObject ja=response.getGraphObject().getInnerJSONObject();
		            	jsondata=ja.toString();
		            	JSONArray feedArr = ja.getJSONArray("data");
		            	 
		            	for(int i=0;i<feedArr.length();i++)
		            	{
		            	 	   JSONObject item = feedArr.getJSONObject(i);
		                       
		            		url.add(item.getString("source"));
		            	System.out.println(item.getString("source"));
			         	}
		     		  
		                
		            
		                 	 
		           
		            } catch(Exception e) {
		                e.printStackTrace();
		            } 
		        	     
		        	   }
		        	   else
		        		   System.out.println(response.getError());
		        	   //Log.i(TAG, "Result: " + response.toString());
		           }                  
		   });  
		   Request.executeAndWait(request); 
		pdia.dismiss();
		
		       
	}
	
@Override
public void onBackPressed() {
	// TODO Auto-generated method stub
	super.onBackPressed();
}
 
}