package com.infoassembly;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.example.infoassembly.loader.ImageLoader;
import com.example.infoassembly.views.ScaleImageView;

public class StaggeredAdapter extends ArrayAdapter<String> {

	private static ImageLoader mLoader;

	public StaggeredAdapter(Context context, int textViewResourceId,
			ArrayList<String> urls,String albname) {
		super(context, textViewResourceId, urls);
		
		mLoader = new ImageLoader(context,albname);
		
	}


	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		ViewHolder holder;

		if (convertView == null) {
			LayoutInflater layoutInflator = LayoutInflater.from(getContext());
			convertView = layoutInflator.inflate(R.layout.row_staggered_demo,
					null);
			holder = new ViewHolder();
			holder.imageView = (ScaleImageView) convertView .findViewById(R.id.imageView1);
			convertView.setTag(holder);
		}

		holder = (ViewHolder) convertView.getTag();

		mLoader.DisplayImage(getItem(position), holder.imageView);

		return convertView;
	}
static void ClearMap()
{
//mLoader.clearEntries();
}
	static class ViewHolder {
		ScaleImageView imageView;
	}
	
}
