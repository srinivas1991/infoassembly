package com.infoassembly;
import java.util.ArrayList;
import java.util.Arrays;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener; 
import android.widget.Button;
import android.widget.Toast; 

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.facebook.HttpMethod;
import com.facebook.Request.GraphUserCallback;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;
import com.facebook.widget.LoginButton;

public class LoginActivity extends Activity implements OnClickListener {
	 
	    public String greeting;

	  
    private String TAG = "LoginActivity";
	private UiLifecycleHelper uiHelper;
	private LoginButton authButton;
	private ProgressDialog pdia;

	SharedPreferences prefs;
	private String urls[] = { 
	"http://farm7.staticflickr.com/6101/6853156632_6374976d38_c.jpg",
	"http://farm8.staticflickr.com/7232/6913504132_a0fce67a0e_c.jpg",
	"http://farm5.staticflickr.com/4133/5096108108_df62764fcc_b.jpg",
	"http://farm5.staticflickr.com/4074/4789681330_2e30dfcacb_b.jpg",
	"http://farm9.staticflickr.com/8208/8219397252_a04e2184b2.jpg",
	"http://farm9.staticflickr.com/8483/8218023445_02037c8fda.jpg",
	"http://farm9.staticflickr.com/8335/8144074340_38a4c622ab.jpg",
	"http://farm9.staticflickr.com/8060/8173387478_a117990661.jpg",
	"http://farm9.staticflickr.com/8056/8144042175_28c3564cd3.jpg",
	"http://farm9.staticflickr.com/8183/8088373701_c9281fc202.jpg",
	"http://farm9.staticflickr.com/8185/8081514424_270630b7a5.jpg",
	"http://farm9.staticflickr.com/8462/8005636463_0cb4ea6be2.jpg",
	"http://farm9.staticflickr.com/8306/7987149886_6535bf7055.jpg",
	"http://farm9.staticflickr.com/8444/7947923460_18ffdce3a5.jpg",
	"http://farm9.staticflickr.com/8182/7941954368_3c88ba4a28.jpg",
	"http://farm9.staticflickr.com/8304/7832284992_244762c43d.jpg",
	"http://farm9.staticflickr.com/8163/7709112696_3c7149a90a.jpg",
	"http://farm8.staticflickr.com/7127/7675112872_e92b1dbe35.jpg",
	"http://farm8.staticflickr.com/7111/7429651528_a23ebb0b8c.jpg",
	"http://farm9.staticflickr.com/8288/7525381378_aa2917fa0e.jpg",
	"http://farm6.staticflickr.com/5336/7384863678_5ef87814fe.jpg",
	"http://farm8.staticflickr.com/7102/7179457127_36e1cbaab7.jpg",
	"http://farm8.staticflickr.com/7086/7238812536_1334d78c05.jpg",
	"http://farm8.staticflickr.com/7243/7193236466_33a37765a4.jpg",
	"http://farm8.staticflickr.com/7251/7059629417_e0e96a4c46.jpg",
	"http://farm8.staticflickr.com/7084/6885444694_6272874cfc.jpg",
	"http://farm7.staticflickr.com/6101/6853156632_6374976d38_c.jpg",
	"http://farm8.staticflickr.com/7232/6913504132_a0fce67a0e_c.jpg",
	"http://farm5.staticflickr.com/4133/5096108108_df62764fcc_b.jpg",
	"http://farm5.staticflickr.com/4074/4789681330_2e30dfcacb_b.jpg",
	"http://farm9.staticflickr.com/8208/8219397252_a04e2184b2.jpg",
	"http://farm9.staticflickr.com/8483/8218023445_02037c8fda.jpg",
	"http://farm9.staticflickr.com/8335/8144074340_38a4c622ab.jpg",
	"http://farm9.staticflickr.com/8060/8173387478_a117990661.jpg",
	"http://farm9.staticflickr.com/8056/8144042175_28c3564cd3.jpg",
	"http://farm9.staticflickr.com/8183/8088373701_c9281fc202.jpg",
	"http://farm9.staticflickr.com/8185/8081514424_270630b7a5.jpg",
	"http://farm9.staticflickr.com/8462/8005636463_0cb4ea6be2.jpg",
	"http://farm9.staticflickr.com/8306/7987149886_6535bf7055.jpg",
	"http://farm9.staticflickr.com/8444/7947923460_18ffdce3a5.jpg",
	"http://farm9.staticflickr.com/8182/7941954368_3c88ba4a28.jpg",
	"http://farm9.staticflickr.com/8304/7832284992_244762c43d.jpg",
	"http://farm9.staticflickr.com/8163/7709112696_3c7149a90a.jpg",
	"http://farm8.staticflickr.com/7127/7675112872_e92b1dbe35.jpg",
	"http://farm8.staticflickr.com/7111/7429651528_a23ebb0b8c.jpg",
	"http://farm9.staticflickr.com/8288/7525381378_aa2917fa0e.jpg",
	"http://farm6.staticflickr.com/5336/7384863678_5ef87814fe.jpg",
	"http://farm8.staticflickr.com/7102/7179457127_36e1cbaab7.jpg",
	"http://farm8.staticflickr.com/7086/7238812536_1334d78c05.jpg",
	"http://farm8.staticflickr.com/7243/7193236466_33a37765a4.jpg",
	"http://farm8.staticflickr.com/7251/7059629417_e0e96a4c46.jpg",
	"http://farm8.staticflickr.com/7084/6885444694_6272874cfc.jpg"};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	
		uiHelper = new UiLifecycleHelper(this, callback);
	    uiHelper.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		authButton = (LoginButton) findViewById(R.id.authButton);
		authButton.setText("Login");
		authButton.setReadPermissions(Arrays.asList("email", "user_photos","friends_photos"));
		setOnClickListeners();
Button btn=(Button)findViewById(R.id.btn);
btn.setOnClickListener(this);
	}

	private void setOnClickListeners() {
		// findViewById(R.id.facebookButton).setOnClickListener(this);
		}
	
	private Session.StatusCallback callback = new Session.StatusCallback() {
	    @Override
	    public void call(Session session, SessionState state, Exception exception) {
	        onSessionStateChange(session, state, exception);
	    }
	};
 
	private void onSessionStateChange(Session session, SessionState state,
			Exception exception) {
		
		if (session.isOpened()) {
			Log.d(TAG,"AccessToken :"+session.getAccessToken());
			Toast.makeText(this, "Logged in...", Toast.LENGTH_SHORT).show();
			authButton.setText("");
			pdia = new ProgressDialog(this);
			pdia.setCancelable(true);
			pdia.setMessage("retrieving URLS..");
			pdia.show();
             getUserData();
		}

	}

	private void getAlbums() {
				Session session = Session.getActiveSession();
		   Request request = new Request(session,
		       "/me/albums",                         
		       null,                        
		       HttpMethod.GET,                 
		       new Request.Callback(){         
		           public void onCompleted(Response response) {
		        	   if(response.getError()==null)
		        	   {
		       //    ArrayList<String> l=new ArrayList<String>();
		           ArrayList<String> albumid=new ArrayList<String>();
	            	   try { 
		            	JSONObject ja=response.getGraphObject().getInnerJSONObject();
		            	
		            	JSONArray feedArr = ja.getJSONArray("data");
		            	for(int i=0;i<feedArr.length();i++)
		            	{
		            	 	   JSONObject item = feedArr.getJSONObject(i);
		            	 	   if(item.getString("type").equalsIgnoreCase("profile"))
		            	 	   {
		            		//l.add(item.getString("name"));
		            		   albumid.add(item.getString("id"));

		            	 	   }
		            	}
		     		  
		            	Intent i=new Intent().setClass(LoginActivity.this,AlbumList.class);
		            	//i.putStringArrayListExtra("albums",l);
		            	i.putStringArrayListExtra("albumid",albumid);
		            	i.putExtra("type",0);
			            
		            	pdia.dismiss();
		            	startActivity(i);
		           
		                 	 
		           
		            } catch(Exception e) {
		                e.printStackTrace();
		            }   
		        	   }
		        	   else
		        		   System.out.println(response.getError());
		        	   //Log.i(TAG, "Result: " + response.toString());
		           }                  
		   });  
		   Request.executeAndWait(request); 
		
	}

		@SuppressWarnings("deprecation")
	public void getUserData() {
		Request.executeMeRequestAsync(Session.getActiveSession(),
				new GraphUserCallback() {
  
				@Override
					public void onCompleted(GraphUser user, Response response) {
					if(response.getError()==null)
					{
						getAlbums();
					}else{
						Log.d("facebookerror",response.getError().getErrorMessage());
						Toast.makeText(LoginActivity.this,response.getError().toString(),Toast.LENGTH_LONG).show();
		        	}
				}
				}); 
	}  
 
	@Override
	public void onResume() {
	    super.onResume();
	    uiHelper.onResume();
	//    getAlbums();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
	    super.onActivityResult(requestCode, resultCode, data);
	    uiHelper.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public void onPause() {
	    super.onPause();
	    uiHelper.onPause();
	}

	@Override
	public void onDestroy() {
	    super.onDestroy();
	    uiHelper.onDestroy();
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
	    super.onSaveInstanceState(outState);
	    uiHelper.onSaveInstanceState(outState);
	}

	@Override
	public void onClick(View view) {
		switch(view.getId())
		{
		case R.id.btn:getgoogle();break;
		}
		}

	private void getgoogle() {

		Intent i=new Intent().setClass(LoginActivity.this,AlbumList.class);
    	//i.putStringArrayListExtra("albums",l);
    	i.putExtra("urls", urls);
    	i.putExtra("type",1);
        
    	startActivity(i);
   

	}

}