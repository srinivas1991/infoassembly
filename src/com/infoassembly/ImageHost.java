package com.infoassembly;


import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

public class ImageHost extends Activity{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
	setContentView(R.layout.full_image);
	ViewGroup v=(ViewGroup)findViewById(R.id.lin);
	v.setVisibility(View.INVISIBLE);
	new DownloadImageTask((ImageView) findViewById(R.id.full_image_view),this,v)
    .execute(getIntent().getStringExtra("url"));
	
}
}
